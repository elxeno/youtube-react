import React, { Component } from "react";
import ReactPlayer from "react-player";
import "./App.css";

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;
const clipboard = electron.clipboard;
const getVideoId = window.require("get-video-id");

/* const helmet = window.require("react-helmet");
const Helmet = helmet.Helmet; */

const Sortable = window.require("react-sortable-hoc");
const SortableContainer = Sortable.SortableContainer;
const SortableElement = Sortable.SortableElement;
const arrayMove = Sortable.arrayMove;
/* const HorizontalScroll = window.require("react-scroll-horizontal"); */

const SortableItem = SortableElement(props => {
  let { id, service } = getVideoId(props.item.url);
  return (
    <div
      className={
        "break listItem w-24 text-xs text-center " +
        (props.url === props.item.url ? "active" : "")
      }
      onClick={() => props.clickfn(props.item.url)}
    >
      <div
        className="closebtn"
        onClick={e => {
          e.stopPropagation();
          props.closefn(props.item.url);
        }}
      >
        X
      </div>

      {props.item.solved ? (
        <div height="100%" width="100%">
          <div>
            <img src={props.item.img} />
          </div>
          <div className="font-bold">{props.item.title}</div>
          <div>{props.item.desc}</div>
          <div className="length">{props.item.length}</div>
        </div>
      ) : (
        <div>
          {id && service === "youtube" ? (
            <div width="100%">
              <button
                className="btn"
                onClick={e => {
                  e.stopPropagation();
                  ipcRenderer.send("solve-id", id);
                }}
              >
                Reload
              </button>
            </div>
          ) : (
            ""
          )}

          <div>{props.item.url}</div>
        </div>
      )}
    </div>
  );
});

const SortableList = SortableContainer(props => {
  return (
    <ul className="videolist block p-0">
      {props.list.map((item, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          item={item}
          url={props.url}
          clickfn={props.clickfn}
          closefn={props.closefn}
        />
      ))}
    </ul>
  );
});

class App extends Component {
  constructor(props) {
    super(props);

    //this.onUnload = this.onUnload.bind(this); // if you need to bind callback to this
  }

  state = {
    url: null,
    playing: false,
    volume: 0.5,
    muted: false,
    played: 0,
    loaded: 0,
    duration: 0,
    playbackRate: 1,
    loop: false,
    list: [],
    drag: false,
    showmenu: true,
    ended: false,
    enablesub: true,
    config: {
      youtube: {
        playerVars: {
          cc_load_policy: 1,
          fs: 0,
          hl: "en",
          showinfo: 1,
          rel: 1
        }
      }
    }
  };

  componentDidMount() {
    document.addEventListener("webkitfullscreenchange", () => {
      this.handleFullScreenChange();
      console.log("fs");
    });
  }
  componentWillMount() {
    ipcRenderer.on("dbstate", (e, data) => this.setState(JSON.parse(data)));
    ipcRenderer.send("getstate");
    ipcRenderer.on("open-file", this.addVid);

    ipcRenderer.on("solved-id", (e, data) => {
      let newItem = JSON.parse(data);
      let templist = this.state.list.map(item => {
        if (item.url === newItem.url) {
          newItem.active = item.active;
          return newItem;
        } else {
          return item;
        }
      });
      this.setState({ list: templist });
    });
    ipcRenderer.on("fullscreen", function() {
      console.log("fullscreen msg");
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.list !== prevState.list ||
      this.state.url !== prevState.url ||
      this.state.playbackRate !== prevState.playbackRate ||
      this.state.enablesub !== prevState.enablesub
    ) {
      console.log("----------");
      console.log(prevState.url);
      console.log(prevState.playbackRate);
      console.log(prevState.enablesub);
      console.log(prevState.list);
      console.log("------");
      ipcRenderer.send("setstate", JSON.stringify(this.state));
    }
  }
  componentWillUnmount() {}

  handleFullScreenChange = () => {
    console.log("handleFullScreenChange");
    ipcRenderer.send("fsentered");

    /*     e.preventDefault();
    console.log("e ")
    console.log("fullscreen");
    console.log(document.fullscreenElement || "fselement");
    console.log(document.webkitFullScreenElement || "webkit");
    console.log(document.webkitIsFullscreen ); */
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState({
      list: arrayMove(this.state.list, oldIndex, newIndex)
    });
  };
  solveId = url => {
    let { id, service } = getVideoId(url);
    if (id && service === "youtube") {
      ipcRenderer.send("solve-id", id);
    }
  };

  load = url => {
    this.setState({
      url,
      played: 0,
      loaded: 0,
      playing: true
    });
    //this.setActive(url);
  };
  addVid = (event, newvid) => {
    console.log("addvid");
    console.log(newvid);
    let re = /^file:\/\/.*(mkv|mp4)/i;
    let templist = this.state.list;
    console.log(templist);
    if (
      (ReactPlayer.canPlay(newvid) || re.test(newvid)) &&
      templist.findIndex(item => item.url === newvid) === -1
    ) {
      templist.push({ url: newvid });
      this.setState({ list: templist });
      this.solveId(newvid);
      if (this.state.list.length === 1 || this.state.ended) {
        setTimeout(this.load(newvid), 1000);
      }
    }
  };

  delVid = url => {
    let templist = this.state.list.filter(
      item => (item.url !== url ? true : false)
    );
    this.setState({ list: templist });
  };
  playNext = del => {
    let index = this.state.list.findIndex(item => item.url === this.state.url);
    console.log("playnext");
    if (index < this.state.list.length - 1) {
      this.setState({ url: this.state.list[index + 1].url });

      if (del) {
        let templist = this.state.list;
        templist.splice(index, 1);
        this.setState({ list: templist });
      }
    } else {
      this.setState({ ended: true });
    }
  };
  playPause = () => {
    this.setState({ playing: !this.state.playing });
  };
  stop = () => {
    this.setState({ url: null, playing: false });
  };
  toggleLoop = () => {
    this.setState({ loop: !this.state.loop });
  };
  setVolume = e => {
    this.setState({ volume: parseFloat(e.target.value) });
  };
  toggleMuted = () => {
    this.setState({ muted: !this.state.muted });
  };
  setPlaybackRate = e => {
    this.setState({ playbackRate: parseFloat(e.target.value) });
  };
  onPlay = () => {
    console.log("onPlay");
    /* this.updateVolume(); */
    this.handleSub(this.state.enablesub);
    this.setState({ ended: false });
  };
  handleSub = enable => {
    let { id, service } = getVideoId(this.state.url);
    if (id && service === "youtube" && this.state.started) {
      if (enable) {
        this.player
          .getInternalPlayer()
          .setOption("captions", "track", { languageCode: "en" });
        this.setState({ enablesub: true });

        console.log("showsub");
      } else {
        this.player.getInternalPlayer().unloadModule("captions");
        this.setState({ enablesub: false });
        console.log("hidesub");
      }
    }
  };
  onPause = () => {
    console.log("onPause");
    this.setState({ playing: false });
  };
  onReady = () => {
    this.setState({ playing: true });
  };
  onSeekMouseDown = e => {
    this.setState({ seeking: true });
  };
  onSeekChange = e => {
    this.setState({ played: parseFloat(e.target.value) });
  };
  onSeekMouseUp = e => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };
  updateVolume = () => {
    let { id, service } = getVideoId(this.state.url);
    if (id && service === "youtube") {
      let vol = this.player.getInternalPlayer().getVolume();
      vol = vol / 100;
      console.log("volume " + vol);
      this.setState({ volume: vol });
    }
  };
  onProgress = state => {
    //console.log('onProgress', state)
    // We only want to update time slider if we are not currently seeking
    /* this.updateVolume(); */
    if (!this.state.seeking) {
      this.setState(state);
    }
  };
  onStart = () => {
    console.log("onStart");
    this.setState({ started: true });
    this.handleSub(this.state.enablesub);
  };
  onEnded = () => {
    console.log("onEnded");
    console.log("end");
    this.playNext(false);
  };
  onDuration = duration => {
    console.log("onDuration", duration);
    this.setState({ duration });
  };
  /* 	onClickFullscreen = () => {
	  screenfull.request(findDOMNode(this.player))
	} */
  renderLoadButton = (url, label) => {
    return <button onClick={() => this.load(url)}>{label}</button>;
  };
  ref = player => {
    this.player = player;
    console.log("player");
  };

  render() {
    const {
      url,
      playing,
      volume,
      muted,
      loop,
      played,
      loaded,
      duration,
      playbackRate
    } = this.state;
    return (
      <div className="comp absolute pin bg-black m-0 p-0">
        <div
          className={
            "menu absolute font-sans text-sm m-0 p-0" +
            (this.state.showmenu ? "" : " menu-hidden")
          }
        >
          <div className="addbox">
            <input
              className="volslider"
              type="range"
              min="0"
              max="1"
              step="0.01"
              value={volume}
              onChange={e => {
                this.setState({ volume: e.target.value });
              }}
            />
            <div
              className="btn"
              onClick={() => {
                console.log(clipboard.readText());
                this.addVid("", clipboard.readText());
              }}
            >
              Add From Clipboard
            </div>
            <div
              className={
                "btn " +
                (this.state.playbackRate === 0.75 ? "speed-active" : "")
              }
              onClick={() => {
                this.setState({ playbackRate: 0.75 });
              }}
            >
              0.75x
            </div>
            <div
              className={
                "btn " + (this.state.playbackRate === 1 ? "speed-active" : "")
              }
              onClick={() => {
                this.setState({ playbackRate: 1 });
              }}
            >
              {" "}
              1x{" "}
            </div>
            <div
              className={
                "btn " +
                (this.state.playbackRate === 1.25 ? "speed-active" : "")
              }
              onClick={() => {
                this.setState({ playbackRate: 1.25 });
              }}
            >
              1.25x
            </div>
            <div
              className={
                "btn " + (this.state.playbackRate === 1.5 ? "speed-active" : "")
              }
              onClick={() => {
                this.setState({ playbackRate: 1.5 });
              }}
            >
              1.5x
            </div>
            <div
              className={
                "btn " + (this.state.playbackRate === 2 ? "speed-active" : "")
              }
              onClick={() => {
                this.setState({ playbackRate: 2 });
              }}
            >
              2x
            </div>
            <div
              className="btn "
              onClick={() => {
                this.setState({ showmenu: !this.state.showmenu });
              }}
            >
              {this.state.showmenu ? "Hide" : "Show"} Menu{" "}
            </div>

            <div
              className="btn "
              onClick={() => {
                this.handleSub(!this.state.enablesub);
              }}
            >
              {this.state.enablesub ? "Disable" : "Enable"} Sub
            </div>
            <div
              className="progress-played"
              style={{
                width: Math.round(this.state.played * window.innerWidth) + "px"
              }}
            />
          </div>
          {/* <HorizontalScroll> */}
          <SortableList
            distance={20}
            axis="x"
            lockAxis="x"
            list={this.state.list}
            url={this.state.url}
            onSortEnd={this.onSortEnd}
            clickfn={this.load}
            closefn={this.delVid}
            className="videolist block p-0"
          />
          {/* </HorizontalScroll> */}
        </div>
        <div
          className={
            "player-wrapper absolute bg-blue-darker" +
            (this.state.showmenu ? "" : " no-menu")
          }
        >
          <ReactPlayer
            controls
            autoPlay
            ref={this.ref}
            className="react-player"
            height="100%"
            width="100%"
            url={url}
            playing={playing}
            loop={loop}
            playbackRate={playbackRate}
            volume={volume}
            muted={muted}
            onReady={this.onReady}
            onStart={this.onStart}
            onPlay={this.onPlay}
            onPause={this.onPause}
            onBuffer={() => console.log("onBuffer")}
            onSeek={e => console.log("onSeek", e)}
            onEnded={this.onEnded}
            onError={e => console.log("onError", e)}
            onProgress={this.onProgress}
            onDuration={this.onDuration}
            config={this.state.config}
          />
        </div>
      </div>
    );
  }
}

App.propTypes = {};

export default App;
