const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;

const path = require("path");
const url = require("url");
const isDev = require("electron-is-dev");

const request = require("request");
const cheerio = require("cheerio");

const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

let state;
!db.getState() ? db.setState({}) : "";
state = db.getState();
let counter = 0 ;
let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    webPreferences: {
      webSecurity: false
    }
  });
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("closed", () => (mainWindow = null));

  mainWindow.webContents.on("will-navigate", function(e, url) {
    e.preventDefault();
    mainWindow.webContents.send("open-file", url);
  });
  mainWindow.webContents.on("new-window", function(e, url) {
    e.preventDefault();
    mainWindow.webContents.send("open-file", url);
  });
  ipcMain.on("fsentered", () => {
    console.log("received fs msg");
    console.log(mainWindow.isFullScreen());
    console.log(mainWindow.isSimpleFullScreen());
   /*  setTimeout(() => {
      mainWindow.setFullScreen(false);
    }, 3000);
    setTimeout(() => {
      mainWindow.setFullScreen(true);
    }, 6000); */
  });
}

function getVideoData(id) {
  let url = "https://www.youtube.com/watch?v=" + id;
  request(url, function(error, response, html) {
    if (!error && response.statusCode == 200) {
      let $ = cheerio.load(html);
      let duration = $('[itemprop="duration"]').attr("content") || "00M00S";
      let min = duration.substring(
        duration.indexOf("PT") + 2,
        duration.indexOf("M")
      );
      let sec = duration.substring(
        duration.indexOf("M") + 1,
        duration.indexOf("S")
      );
      if (sec.length == 1) {
        sec = "0" + sec;
      }

      let temp = {
        url: url,
        title: $('[itemprop="name"]').attr("content") || "Title not found",
        desc: $('[itemprop="description"]').attr("content") || "No Description",
        img: "https://img.youtube.com/vi/" + id + "/default.jpg",
        length: min + ":" + sec,
        pub: $('[itemprop="datePublished"]').attr("content") || "",
        channelid: $('[itemprop="channelId"]').attr("content") || "",
        channelname: $(".yt-user-info > a").text() || "",
        solved: true
      };
      //console.log(temp);
      mainWindow.webContents.send("solved-id", JSON.stringify(temp));
    }
  });
}

//Listeners
ipcMain.on("solve-id", (e, data) => getVideoData(data));

ipcMain.on("mainfs", () => {
  mainWindow.setFullScreen(!mainWindow.isFullScreen());
  mainWindow.webContents.send("fullscreen");
});
ipcMain.on("setstate", (e, data) => {
  console.log("saving state" + counter++);
  db.setState(JSON.parse(data)).write();
});
ipcMain.on("getstate", () => {
  console.log("getstate");
  mainWindow.webContents.send("dbstate", JSON.stringify(db.getState()));
});

//

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
